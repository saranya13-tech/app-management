from django.urls import path
from . import views

urlpatterns = [
    # ... other URL patterns ...
    path('admin/', views.admin_home, name='admin_home'),
    path('admin/add_app/', views.add_android_app, name='add_android_app'),
    path('user/dashboard/', views.user_dashboard, name='user_dashboard'),
    path('user/upload_screenshot/', views.upload_screenshot, name='upload_screenshot'),
    path('user/profile/', views.user_profile, name='user_profile'),
]
