from django.shortcuts import render, redirect
from .forms import AndroidAppForm 
from app.models import AndroidApp
from django.http import HttpResponse
from app.models import App, Screenshot  

def admin_home(request):
    
    android_apps = AndroidApp.objects.all()

    total_points = sum(app.points for app in android_apps)

    context = {
        'android_apps': android_apps,
        'total_points': total_points,
    }

    return render(request, 'admin/admin_home.html', context)




def add_android_app(request):
    if request.method == 'POST':
        # Create a form instance with the POST data
        form = AndroidAppForm(request.POST)

        # Check if the form is valid
        if form.is_valid():
            # If the form is valid, save the data to the database
            app = form.save()

            # Redirect to the admin home page after successful submission
            return redirect('admin_home')

    else:
        # If it's a GET request or the form is invalid, display the form
        form = AndroidAppForm()

    return render(request, 'admin/add_android_app.html', {'form': form})



def user_dashboard(request):
    # Fetch apps, points, and tasks for the user from the database
    user = UserProfile.objects.filter(user=request.user)
    user_tasks = Screenshot.objects.filter(user=request.user)

    context = {
        'user': user,
        'user_tasks': user_tasks,
    }

    return render(request, 'user/user_dashboard.html', context)



def upload_screenshot(request):
    if request.method == 'POST':
        # Get the uploaded file
        uploaded_file = request.FILES.get('screenshot')

        if uploaded_file:
            
            # Save the uploaded file to the media directory
            with open(os.path.join(settings.MEDIA_ROOT, uploaded_file.name), 'wb') as destination:
                for chunk in uploaded_file.chunks():
                    destination.write(chunk)

            return HttpResponse('File uploaded successfully.')

    return render(request, 'upload_screenshot.html')


def user_profile(request):
    user = request.user  

    context = {
        'user': user,
    }

    return render(request, 'user_profile.html', context)