from django.urls import path
from . import views
from rest_framework.routers import DefaultRouter
router.register('android-apps', views.AndroidAppViewSet)
router.register('user-profiles', views.UserProfileViewSet)
router.register('screenshots', views.ScreenshotViewSet)
urlpatterns = [
    # ... other URL patterns ...
    path('signup/', views.signup, name='signup'),
    path('login/', views.user_login, name='login'),
    path('api/', include(router.urls)),
]
