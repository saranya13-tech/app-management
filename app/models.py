from django.db import models

# Create your models here.

from django.contrib.auth.models import User
from django.db import models

class AndroidApp(models.Model):
    name = models.CharField(max_length=255)
    points = models.PositiveIntegerField()

class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    points_earned = models.PositiveIntegerField(default=0)
    tasks_completed = models.PositiveIntegerField(default=0)
    # You can add more fields for the user's profile.

class Screenshot(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    task_description = models.CharField(max_length=255)
    screenshot = models.ImageField(upload_to='screenshots/')
